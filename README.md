```go
data := OpenAPI{
	Version: "3.0.0",
	Info: Info{
		Title:          "Some fake API",
		Description:    "An example API doc",
		TermsOfService: "http://some-fake-service-agreement.co.uk",
		Contact: Contact{
			Email: "sam@whiteteam.co.uk",
		},
		License: License{
			Name: "Some Fake License",
			URL:  "http://some-fake-license.co.uk",
		},
		APIVersion: "0.0.0",
	},
	ExternalDocs: ExternalDocumentation{
		Description: "Some other slightly worse docs",
		URL:         "http://some-worse-docs.co.uk",
	},
	Servers: []Server{
		{
			URL: "http://some-fake-server.co.uk",
		},
	},
	Tags: []Tag{
		{
			Name:        "fake-tag",
			Description: "A fake tag that isn't really useful",
			ExternalDocs: ExternalDocumentation{
				Description: "Some tag docs",
				URL:         "http://some-tag-docs.co.uk",
			},
		},
	},
	Paths: map[string]PathItem{
		"/some/path/{someId}": PathItem{
			Description: "The data to send to some path",
			Summary:     "This is a really terse summary because my fingers hurt",
			Post: Operation{
				Tags:         []string{"fake-tag"},
				Description:  "POST things to some path",
				Summary:      "This is what happens when you POST to this route",
				ExternalDocs: ExternalDocumentation{},
				OperationID:  "post-some-stuff",
				Parameters: []interface{}{
					Parameter{
						Name:            "someId",
						Description:     "A really interesting parameter",
						In:              "path",
						Required:        true,
						Deprecated:      false,
						AllowEmptyValue: false,
						Schema: Schema{
							Type: "string",
						},
						Example: "7bca75bc-01ee-4d7d-81c1-fb7deda60fe8",
					},
				},
				RequestBody: RequestBody{
					Description: "The data to pass to this route",
					Required:    true,
					Content: map[string]MediaType{
						"application/json": MediaType{
							Schema: Schema{
								Type: "object",
								Properties: map[string]*Schema{
									"name": &Schema{
										Type:    "string",
										Example: "sam",
									},
									"password": &Schema{
										Type:    "string",
										Example: "$supEr$eCurP&asSworD",
									},
								},
							},
						},
					},
				},
				Responses: map[string]interface{}{
					"200": Reference{
						Ref: "#/components/responses/tokenOK",
					},
					"404": Reference{
						Ref: "#/components/responses/tokenNotOK",
					},
				},
				CallBacks:  map[string]interface{}{},
				Deprecated: false,
				Security: []map[string][]string{
					{
						"openbook": []string{"read", "write"},
					},
				},
				Servers: []Server{},
			},
		},
	},
	Components: Components{
		Responses: map[string]interface{}{
			"tokenOK": Response{
				Description: "A successful response to this request",
				Content: map[string]MediaType{
					"application/json": MediaType{
						Schema: Schema{
							Type: "object",
							Properties: map[string]*Schema{
								"accessToken": &Schema{
									Type:    "string",
									Example: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
								},
								"refreshToken": &Schema{
									Type:    "string",
									Example: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
								},
								"errors": &Schema{
									Type: "array",
									Items: &Schema{
										Type: "string",
									},
									MaxItems: createUintPointer(0),
									Example:  []string{},
								},
							},
						},
					},
				},
			},
			"tokenNotOK": Response{
				Description: "An unsuccessful response to this request",
				Content: map[string]MediaType{
					"application/json": MediaType{
						Schema: Schema{
							Type: "object",
							Properties: map[string]*Schema{
								"accessToken": &Schema{
									Type:      "string",
									MaxLength: createUintPointer(0),
									Example:   "",
								},
								"refreshToken": &Schema{
									Type:      "string",
									MaxLength: createUintPointer(0),
									Example:   "",
								},
								"errors": &Schema{
									Type: "array",
									Items: &Schema{
										Type:    "string",
										Example: "token could not be created: user not found",
									},
									MinItems: createUintPointer(1),
								},
							},
						},
					},
				},
			},
		},
	},
}

```
