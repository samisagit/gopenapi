package gopenapi

func CreateUintPointer(input uint) *uint {
	return &input
}

type OpenAPI struct {
	Version      string                `yaml:"openapi"`
	Info         Info                  `yaml:"info"`
	ExternalDocs ExternalDocumentation `yaml:"externalDocs,omitempty"`
	Servers      []Server              `yaml:"servers,omitempty"`
	Tags         []Tag                 `yaml:"tags,omitempty"`
	Paths        map[string]PathItem   `yaml:"paths"`
	Components   interface{}           `yaml:"components,omitempty"`
}

type Info struct {
	Title          string  `yaml:"title"`
	Description    string  `yaml:"description,omitempty"`
	TermsOfService string  `yaml:"termsOfService,omitempty"`
	Contact        Contact `yaml:"contact,omitempty"`
	License        License `yaml:"license,omitempty"`
	APIVersion     string  `yaml:"version"`
}

type Contact struct {
	Name  string `yaml:"name,omitempty"`
	URL   string `yaml:"url,omitempty"`
	Email string `yaml:"email,omitempty"`
}

type License struct {
	Name string `yaml:"name"`
	URL  string `yaml:"url,omitempty"`
}

type ExternalDocumentation struct {
	Description string `yaml:"description,omitempty"`
	URL         string `yaml:"url"`
}

type Server struct {
	URL         string                    `yaml:"url"`
	Description string                    `yaml:"description,omitempty"`
	Variables   map[string]ServerVariable `yaml:"variables,omitempty"`
}

type ServerVariable struct {
	Enum        []string `yaml:"enum,omitempty"`
	Default     string   `yaml:"default,omitempty"`
	Description string   `yaml:"description,omitempty"`
}

type Tag struct {
	Name         string                `yaml:"name"`
	Description  string                `yaml:"description,omitempty"`
	ExternalDocs ExternalDocumentation `yaml:"externalDocs,omitempty"`
}

type PathItem struct {
	Ref         string    `yaml:"$ref,omitempty"`
	Summary     string    `yaml:"summary,omitempty"`
	Description string    `yaml:"description,omitempty"`
	Get         Operation `yaml:"get,omitempty"`
	Post        Operation `yaml:"post,omitempty"`
	Patch       Operation `yaml:"patch,omitempty"`
	Put         Operation `yaml:"put,omitempty"`
	Delete      Operation `yaml:"delete,omitempty"`
	Options     Operation `yaml:"options,omitempty"`
	Head        Operation `yaml:"head,omitempty"`
	Trace       Operation `yaml:"trace,omitempty"`
	Servers     []Server  `yaml:"servers,omitempty"`
	// Parameters can be a Parameter or Reference
	Parameters interface{} `yaml:"parameters,omitempty"`
}

type Operation struct {
	Tags         []string              `yaml:"tags,omitempty"`
	Summary      string                `yaml:"summary,omitempty"`
	Description  string                `yaml:"description,omitempty"`
	ExternalDocs ExternalDocumentation `yaml:"externalDocs,omitempty"`
	OperationID  string                `yaml:"operationId,omitempty"`
	// Parameters can be a Parameter or Reference
	Parameters interface{} `yaml:"parameters,omitempty"`
	// RequestBodies can be a RequestBody or Reference
	RequestBody interface{} `yaml:"requestBody,omitempty,omitempty"`
	// Response values can be a Response or a Reference
	Responses map[string]interface{} `yaml:"responses"`
	// Callback values can be a Callback or a Reference
	CallBacks  map[string]interface{}
	Deprecated bool `yaml:"deprecated,omitempty"`
	// Security can be an array or a map of strig of array
	Security interface{} `yaml:"security,omitempty"`
	Servers  []Server    `yaml:"servers,omitempty"`
}

type Parameter struct {
	Name            string `yaml:"name"`
	In              string `yaml:"in"`
	Description     string `yaml:"description,omitempty"`
	Required        bool   `yaml:"required"`
	Deprecated      bool   `yaml:"deprecated,omitempty"`
	AllowEmptyValue bool   `yaml:"allowEmptyValue,omitempty"`
	Style           string `yaml:"style,omitempty"`
	Explode         bool   `yaml:"explode,omitempty"`
	AllowReserved   bool   `yaml:"allowReserved,omitempty"`
	// Schema can be a Schema or an interface
	Schema  interface{} `yaml:"schema,omitempty"`
	Example interface{} `yaml:"example,omitempty"`
	// Example values can be an Example or an interface{}
	Examples map[string]interface{} `yaml:"examples,omitempty"`
	Content  map[string]MediaType   `yaml:"content,omitempty"`
}

type Header struct {
	Description     string `yaml:"description,omitempty"`
	Required        bool   `yaml:"required,omitempty"`
	Deprecated      bool   `yaml:"deprecated,omitempty"`
	AllowEmptyValue bool   `yaml:"allowEmptyValue,omitempty"`
	Style           string `yaml:"style,omitempty"`
	Explode         bool   `yaml:"explode,omitempty"`
	AllowReserved   bool   `yaml:"allowReserved,omitempty"`
	// Schema can be a Schema or an interface
	Schema  interface{} `yaml:"schema,omitempty"`
	Example interface{} `yaml:"example,omitempty"`
	// Example values can be an Example or an interface{}
	Examples map[string]interface{} `yaml:"examples,omitempty"`
	Content  map[string]MediaType   `yaml:"content,omitempty"`
}

type RequestBody struct {
	Description string               `yaml:"description,omitempty"`
	Content     map[string]MediaType `yaml:"content"`
	Required    bool                 `yaml:"required,omitempty"`
}

type Response struct {
	Description string `yaml:"description"`
	// Header values can be a Header or a Reference
	Headers map[string]interface{} `yaml:"headers,omitempty"`
	Content map[string]MediaType   `yaml:"content,omitempty"`
	// Link values can be a Link or a Reference
	Links map[string]interface{} `yaml:"links,omitempty"`
}

type Reference struct {
	Ref string `yaml:"$ref"`
}

type Link struct {
	OperationRef string                 `yaml:"operationRef,omitempty"`
	OperationId  string                 `yaml:"operationId,omitempty"`
	Parameters   map[string]interface{} `yaml:"parameters,omitempty"`
	RequestBody  interface{}            `yaml:"requestBody,omitempty"`
	Description  string                 `yaml:"description,omitempty"`
	Server       Server                 `yaml:"server,omitempty"`
}

type MediaType struct {
	// Example values can be an Schema or a Reference
	Schema  interface{} `yaml:"schema,omitempty"`
	Example interface{} `yaml:"example,omitempty,omitempty"`
	// Example values can be an Example or a Reference
	Examples map[string]interface{} `yaml:"examples,omitempty"`
	Encoding map[string]Encoding    `yaml:"encoding,omitempty"`
}

type Schema struct {
	Title            string        `yaml:"title,omitempty"`
	MultipleOf       *int          `yaml:"multipleOf,omitempty"`
	Maximum          *uint         `yaml:"maximum,omitempty"`
	exclusiveMaximum bool          `yaml:"exclusiveMaximum,omitempty"`
	Minimum          *uint         `yaml:"minimum,omitempty"`
	ExclusiveMinimum bool          `yaml:"exclusiveMinimum,omitempty"`
	MaxLength        *uint         `yaml:"maxLength,omitempty"`
	MinLength        *uint         `yaml:"minLength,omitempty"`
	Pattern          string        `yaml:"pattern,omitempty"`
	MaxItems         *uint         `yaml:"maxItems,omitempty"`
	MinItems         *uint         `yaml:"minItems,omitempty"`
	UniqueItems      bool          `yaml:"uniqueItems,omitempty"`
	MaxProperties    *uint         `yaml:"maxProperties,omitempty"`
	MinProperties    *uint         `yaml:"minProperties,omitempty"`
	Required         []string      `yaml:"required,omitempty"`
	Enum             []interface{} `yaml:"enum,omitempty"`
	Type             string        `yaml:"type"`
	// AllOf items can be a Schema or a Reference
	AllOf []interface{} `yaml:"allOf,omitempty"`
	// AnyOf items can be a Schema or a Reference
	AnyOf []interface{} `yaml:"anyOf,omitempty"`
	// OneOf items can be a Schema or a Reference
	OneOf []interface{} `yaml:"oneOf,omitempty"`
	// Not can be a Schema or a Reference
	Not interface{} `yaml:"not,omitempty"`
	// Item can be a Schema or a Reference
	Items interface{} `yaml:"items,omitempty"`
	// Property values can be a Schema or a Reference
	Properties map[string]interface{} `yaml:"properties,omitempty"`
	// AdditionalProperties can be a Schema or a bool
	AdditionalProperties interface{}           `yaml:"additionalProperties,omitempty"`
	Description          string                `yaml:"description,omitempty"`
	Format               string                `yaml:"format,omitempty"`
	Default              interface{}           `yaml:"default,omitempty"`
	Nullable             bool                  `yaml:"nullable,omitempty"`
	Discriminator        Discriminator         `yaml:"dicriminator,omitempty"`
	ReadOnly             bool                  `yaml:"readOnly,omitempty"`
	WriteOnly            bool                  `yaml:"writeOnly,omitempty"`
	XML                  XML                   `yaml:"xml,omitempty"`
	ExternalDocs         ExternalDocumentation `yaml:"externalDocs,omitempty"`
	Example              interface{}           `yaml:"example,omitempty,omitempty"`
	Deprecated           bool                  `yaml:"deprecated,omitempty"`
}

type Components struct {
	// Schema values can be a Schema or a Reference
	Schemas map[string]interface{} `yaml:"schemas,omitempty"`
	// Response values can be a Response or a Reference
	Responses map[string]interface{} `yaml:"responses,omitempty"`
	// Parameter values can a Parameter or a Reference
	Parameters map[string]interface{} `yaml:"parameters,omitempty"`
	// Example values can be an Example or a Reference
	Examples map[string]interface{} `yaml:"examples,omitempty"`
	// RequestBody values can be a RequestBody or a Reference
	RequestBodies map[string]interface{} `yaml:"requestBodies,omitempty"`
	// Header values can be a Header or a Reference
	Headers map[string]interface{} `yaml:"headers,omitempty"`
	// SecuritySchema values can be a SecuritySchema or a Reference
	SecuritySchemes map[string]interface{} `yaml:"securitySchemes,omitempty"`
	// Link values can be a Link or a Reference
	Links map[string]interface{} `yaml:"links,omitempty"`
	// Callback values can be a Callback or a Reference
	Callbacks map[string]interface{} `yaml:"callbacks,omitempty"`
}

type Discriminator struct {
	PropertyName string            `yaml:"propertyName"`
	Mapping      map[string]string `yaml:"mapping,omitempty"`
}

type XML struct {
	Name      string `yaml:"name,omitempty"`
	Namespace string `yaml:"namespace,omitempty"`
	Prefix    string `yaml:"prefix,omitempty"`
	Attribute bool   `yaml:"attribute,omitempty"`
	Wrapped   bool   `yaml:"wrapped,omitempty"`
}

type SecurityScheme struct {
	Type             string     `yaml:"type"`
	Description      string     `yaml:"description,omitempty"`
	Name             string     `yaml:"name,omitempty"`
	In               string     `yaml:"in,omitempty"`
	Scheme           string     `yaml:"scheme,omitempty"`
	BearerFormat     string     `yaml:"bearerFormat,omitempty"`
	OAuthFlows       OAuthFlows `yaml:"flows,omitempty"`
	OpenIdConnectURL string     `yaml:"openIdConnectUrl,omitempty"`
}

type OAuthFlows struct {
	Implicit          OAuthFlow `yaml:"implicit,omitempty"`
	Password          OAuthFlow `yaml:"password,omitempty"`
	ClientCredentials OAuthFlow `yaml:"clientCredentials,omitempty"`
	Authorization     OAuthFlow `yaml:"athorization,omitempty"`
}

type OAuthFlow struct {
	AuthorizationURL string            `yaml:"authorizationUrl"`
	TokenURL         string            `yaml:"tokeUrl"`
	RefreshURL       string            `yaml:"refreshUrl,omitempty"`
	Scoped           map[string]string `yaml:"scopes"`
}

type Encoding struct {
	ContentType   string                 `yaml:"contentType,omitempty"`
	Headers       map[string]interface{} `yaml:"headers,omitempty"`
	Style         string                 `yaml:"style,omitempty"`
	Explode       bool                   `yaml:"explode,omitempty"`
	AllowReserved bool                   `yaml:"allowReserved,omitempty"`
}
